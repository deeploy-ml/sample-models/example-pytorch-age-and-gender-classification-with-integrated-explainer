import torch
import logging
import os
import json
import io
import base64

from typing import List, Tuple
import numpy as np
from ts.torch_handler.base_handler import BaseHandler
from torch import Tensor
from torchvision import transforms as T
from captum.attr import visualization as viz
from captum.attr import Saliency
from mtcnn import MTCNN
from pred_models import AgeRangeModel
import matplotlib.pyplot as plt


logger = logging.getLogger(__name__)


class ModelHandler(BaseHandler):
    def initialize(self, context):
        """Initialize function loads the model, face detection model and the explainer

        Args:
            context (context): It is a JSON Object containing information
            pertaining to the model artifacts parameters.

        Raises:
            RuntimeError: Raises the Runtime error when the model or
            explainer is missing

        """
        properties = context.system_properties
        self.manifest = context.manifest
        model_dir = properties.get("model_dir")
        logging.info(f"Model dir: {model_dir}")

        # use GPU if available
        self.device = torch.device(
            "cuda:" + str(properties.get("gpu_id"))
            if torch.cuda.is_available() and properties.get("gpu_id") is not None
            else "cpu"
        )
        logger.info(f"Using device {self.device}")

        # load the model
        logger.info("Retrieving model file")
        model_file = self.manifest["model"]["modelFile"]

        logger.info("Retrieving model weights")
        serialized_file = self.manifest["model"]["serializedFile"]
        model_pt_path = os.path.join(model_dir, serialized_file)

        logger.info("Loading model")
        if model_file:
            logger.debug("Loading eager model")
            self.model = self._load_pickled_model(model_dir, model_file, model_pt_path)
            self.model.to(self.device)
            self.model.eval()
            logger.info(f"Successfully loaded model from {model_file}")
        else:
            raise RuntimeError("Missing the model file")

        # # # Load face detection model for preprocessing step
        logger.info("Loading face detection model")
        self.face_detection = MTCNN(device=self.device)
        if self.face_detection is not None:
            logger.info("Successfully initialized face detection model")
        else:
            logger.warning("Face detection model not detected")

        # load mapping file to map model output to understandable labels
        logger.info("Loading label mapping file")
        mapping_file_path = os.path.join(model_dir, "index_to_name.json")
        if os.path.isfile(mapping_file_path):
            with open(mapping_file_path) as f:
                self.mapping = json.load(f)
            logger.info("Successfully loaded mapping file")
        else:
            logger.warning("Mapping file is not detected")

        # load explainer
        logger.info("Loading explainability method")
        self.age_range_model = AgeRangeModel()
        self.saliency = Saliency(self.age_range_model)
        # if self.integrated_gradients is not None and self.noise_tunnel is not None:
        if self.age_range_model is not None:
            logger.info(
                "Successfully initialized age range model to be used for explainability"
            )
        else:
            logger.warning("Model for explainer not detected")
        if self.saliency is not None:
            logger.info("Successfully initialized Saliency explanation method")
        else:
            raise RuntimeError("Missing explainer")

        if self.age_range_model is not None:
            logger.info("Successfully loaded AgeRangeModel for explainability")
        else:
            raise RuntimeError("Missing model for explainability")

        # initialize necessary variables
        self.min_prob = 0.9
        self.face_size = (64, 64)
        self.thickness_per_pixels = 500
        self.mean = torch.tensor([0.5, 0.5, 0.5])
        self.std = torch.tensor([0.5, 0.5, 0.5])

        self.initialized = True

    def preprocess(self, data: List) -> torch.Tensor:
        """The preprocess function first transforms the image to the right size and normalizes it.
            The MTCNN model is then called to detect the face in the image. The face is cropped and
            fed back as a stack of tensors, which is the output of the preprocessing function.
        Args:
            data (List): Input data from the request is in the form of a Tensor

        Returns:
            face_images (torch.Tensor) : The preprocess function returns the input image as a torch.Tensor.
        """
        # Read data and convert to float tensor
        data = data[0]["data"]
        image = torch.FloatTensor(data)

        # Code to show output to validate
        # image_np = np.transpose(image.numpy(), (1, 2, 0))
        # plt.imshow(image_np)
        # plt.axis('off')
        # plt.show()

        # transform image to PILImage to feed to face detection model
        transform2img = T.ToPILImage()
        img = transform2img(image)
        image_shape = image.shape

        # call face detection model to detect face
        bboxes, prob = self.face_detection.detect(img)

        bboxes = bboxes[prob > self.min_prob]

        face_images = []

        # crop image to only be the face to be fed to model
        for box in bboxes:
            box = np.clip(box, 0, np.inf).astype(np.uint32)

            padding = max(image_shape) * 5 / self.thickness_per_pixels

            padding = int(max(padding, 10))

            box = self.padding_face(box, padding)

            face = img.crop(box)
            transformed_face = self.transform(face)
            face_images.append(transformed_face)

        # save images to a stack (in case of multiple faces)
        face_images = torch.stack(face_images, dim=0)

        return face_images

    def inference(self, inputs: Tensor) -> Tensor:
        """Predict class using the model. Predicts both gender and age range.

        Args:
            inputs: tensor of preprocessed image

        Return:
            genders, ages: Tensor, Tensor
        """
        genders, ages = self.model(inputs)

        genders = torch.round(genders)

        ages = torch.round(ages).long()
        return genders, ages

    def postprocess(self, outputs: Tuple[Tensor]) -> List[str]:
        """Convert the output to the string label provided in the label mapper (index_to_name.json)

        Args:
            outputs (list): The tensor outputs produced by the model

        Returns:
            List: a list of the predicted output where the first item is gender and the second is age range
        """
        gender_label = "Man" if outputs[0] == 0 else "Woman"
        age_range = self.mapping[str(outputs[1].item())]
        logger.info(f"PREDICTED LABELS: {gender_label}, {age_range}")

        return [{"gender": gender_label, "age": age_range}]

    def get_insights(self, input: torch.Tensor, _, target) -> dict[List]:
        # TODO: when kserve is updated to support captum 0.7.0 and matplotlib 3.8.0, return commented out section
        """This function calls the Saliency explainer on the input for the AgeRangeModel.
            This function does hence not explain the Gender Model.

        Args:
            input (torch.Tensor): the cropped image detected in the preprocess stage
        Returns:
            Dict[List]: Return a dict with keys "prediction" (the label), "originals" the cropped original image for attribution,
            "explanations" the blended heatmap with the attributions
        """
        try:
            predictions = self.inference(input)
            logger.info(f"Predictions in explainer call {predictions}")
        except Exception as err:
            raise Exception("Failed to predict %s" % err)
        try:
            logger.info("Getting attributions for image")
            attributions_ig_nt = self.saliency.attribute(input, target=predictions[1])
            # attributions_ig_nt = self.noise_tunnel.attribute(input, nt_samples=10, nt_type='smoothgrad_sq', target=pred_label_idx)
            logger.info("Attributions successfully calculated")

            unnormalize = T.Normalize(
                (-self.mean / self.std).tolist(), (1.0 / self.std).tolist()
            )
            unnormalized_img = unnormalize(input.squeeze())
            original_image = np.transpose(
                unnormalized_img.cpu().detach().numpy(), (1, 2, 0)
            )
            attributions = np.transpose(
                attributions_ig_nt.squeeze().cpu().detach().numpy(), (1, 2, 0)
            )
            processed_labels = self.postprocess(predictions)
            processed_prediction = [f"Gender: {processed_labels[0]['gender']}, Age: {processed_labels[0]['age']}"]  # [{"gender": gender_label, "age": age_range}]
            
            response = {}
            response["predictions"] = processed_prediction
            response["originals"] = original_image.tolist()
            response["explanations"] = attributions.tolist()
            logger.info("Explanations successfully obtained")

            """ To be used after Kserve update. Requires Captum 0.7.0 and matplotlib 3.8.0"""
            # response = {}
            # response["prediction"] = self.postprocess(predictions)
            # response["originals"] = self.get_encoded_image(_, original_image, "original")
            # response["explanations"] = self.get_encoded_image(attributions, original_image, "explanation")
            # logger.info("Explanations successfully obtained")
            return response

        except Exception as err:
            raise Exception("Failed to explain %s" % err)

    def get_encoded_image(
        self, attributions: np.ndarray, original_image: np.ndarray, image: str
    ) -> List[dict]:
        """This functions takes the transposed images and creates visualizations that are encoded as b64 strings and then fed back.

        Args:
            attributions (np.array): the attributions from explainability method
            original_image (np.array): the original image without normalization and cropped
            image (str): the type of image to be created, original or the explanation

        Returns:
            List[dict]: a list with the dictionary with prediction, original image and explanation as b64 encoded images
        """
        logger.info(f"Creating figure for {image} image")
        if image == "original":
            # create visualization - set use_pyplot to True when developing if wanting to validate the image
            fig, ax = viz.visualize_image_attr(
                None,
                original_image,
                method="original_image",
                fig_size=(18, 6),
                use_pyplot=True,
            )
        elif image == "explanation":
            fig, ax = viz.visualize_image_attr(
                attributions,
                original_image,
                method="blended_heat_map",
                sign="absolute_value",
                fig_size=(18, 6),
                use_pyplot=True,
            )
        else:
            raise ValueError(
                "image parameter must be either 'original' or 'explanation'"
            )

        # save as base64
        memdata = io.BytesIO()
        fig.savefig(memdata, format="png", bbox_inches="tight")
        memdata.seek(0)
        encoded_string = base64.b64encode(memdata.read())
        encoded_fig = encoded_string.decode("utf-8")
        return [{"b64": encoded_fig}]

    # transform function for preprocessing step
    def transform(self, image):
        return T.Compose(
            [
                T.Resize(self.face_size),
                T.ToTensor(),
                T.Normalize(mean=self.mean, std=self.std),
            ]
        )(image)

    # padding function for preprocessing step
    def padding_face(self, box, padding=10):
        return [box[0] - padding, box[1] - padding, box[2] + padding, box[3] + padding]
