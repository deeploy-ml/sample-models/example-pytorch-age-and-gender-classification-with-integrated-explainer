from torch import nn
import torch
from pred_models import GenderClassificationModel, AgeRangeModel


class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()

        self.gender_model = GenderClassificationModel()

        self.age_range_model = AgeRangeModel()

    def forward(self, x):
        """x: batch, 3, 64, 64"""
        if len(x.shape) == 3:
            x = x[None, ...]

        predicted_genders = self.gender_model(x)

        age_ranges = self.age_range_model(x)

        y = torch.argmax(age_ranges, dim=1).view(
            -1,
        )

        return predicted_genders, y
