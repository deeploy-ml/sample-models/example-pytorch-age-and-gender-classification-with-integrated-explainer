from deeploy import Client
import json

deployment_id = "YOUR DEPLOYMENT ID"

# initialize Deeploy Client
client = Client(
    # if using the Deeploy cloud version 'host' is 'app.deeploy.ml'
    host="app.deeploy.ml",
    workspace_id="YOUR WORKSPACE ID",
    deployment_token="YOUR DEPLOYEMENT TOKEN",
)

# load example request
with open("example_request_person1.json", "r") as f:
    request_body = json.load(f)

# Prediction call
prediction = client.predict(deployment_id, request_body)
print(prediction)

# Explanation call
# explanation = client.explain(deployment_id, request_body)
# print(explanation)
